public class StockData {
    private double stockOpen;
    private double stockHigh;
    private double stockLow;
    private double stockClose;
    private double stockVolume;
    private double stockAfterHours;
    private double stockPreMarket;


    public double getStockOpen() {
        return stockOpen;
    }

    public void setStockOpen(double stockOpen) {
        this.stockOpen = stockOpen;
    }

    public double getStockHigh() {
        return stockHigh;
    }

    public void setStockHigh(double stockHigh) {
        this.stockHigh = stockHigh;
    }

    public double getStockLow() {
        return stockLow;
    }

    public void setStockLow(double stockLow) {
        this.stockLow = stockLow;
    }

    public double getStockClose() {
        return stockClose;
    }

    public void setStockClose(double stockClose) {
        this.stockClose = stockClose;
    }

    public double getStockVolume() {
        return stockVolume;
    }

    public void setStockVolume(double stockVolume) {
        this.stockVolume = stockVolume;
    }

    public double getStockAfterHours() {
        return stockAfterHours;
    }

    public void setStockAfterHours(double stockAfterHours) {
        this.stockAfterHours = stockAfterHours;
    }

    public double getStockPreMarket() {
        return stockPreMarket;
    }

    public void setStockPreMarket(double stockPreMarket) {
        this.stockPreMarket = stockPreMarket;
    }


}
