import javax.script.ScriptEngine;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;


public class Main {
    private ScriptEngine engine;
    private static void createAndShowGUI() {
        //Create and set up the window.
        JFrame frame = new JFrame("Applicazione in borsa");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add the ubiquitous "Hello World" label.
        JLabel label = new JLabel("Chiudere questa finestra per uscire dal programma");
        frame.getContentPane().add(label);

        //Display the window.
        //frame.pack();
        frame.setSize(400,300);
        frame.setVisible(true);
    }
    public static void main(String[] args) throws IOException {
        try {
            createAndShowGUI();
            RestQuery rest = new RestQuery();
            InputData id = new InputData();
            rest.readInputData(id);
            StockInfoData sid = new StockInfoData();
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            CalcData cd = new CalcData();
            Calc calc = new Calc();
            rest.readStockInfo(sid, id.getTicker());
            calc.calcValues(id.getTicker(), id.getStartingDate(), id.getEndingDate(), cd, sid);
        } catch (Exception e){
            System.out.println("BORSA CHIUSA");
        }

    }
}