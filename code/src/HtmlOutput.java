import java.awt.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class HtmlOutput {
    public void writeHtml(File f, CalcData cd,String ticker,StockInfoData sid, Calendar c) throws Exception {
        BufferedWriter bw = new BufferedWriter(new FileWriter(f));

        bw.write("<!DOCTYPE html>\n" +
                "\n" +
                "<html lang=\"en\" dir=\"ltr\">\n" +
                "  <head>\n" +
                "    <meta charset=\"utf-8\">\n" +
                "\n" +
                "    <style media=\"screen\">\n" +
                "      *,*:after,*:before{\n" +
                "        text-align: left;\n" +
                "        -webkit-box-sizing: border-box;\n" +
                "        -moz-box-sizing: border-box;\n" +
                "        -ms-box-sizing: border-box;\n" +
                "        box-sizing:border-box;\n" +
                "      }\n" +
                "      body{\n" +
                "        font-family: arial;\n" +
                "        font-size: 16px;\n" +
                "        margin: 0;\n" +
                "        background: #fff\n" +
                "        color: #000;\n" +
                "        display: flex;\n" +
                "        justify-content: space-around;\n" +
                "        align-items:center;\n" +
                "        min-height: 100vh;\n" +
                "      }\n" +
                "      img {\n" +
                "        border-bottom-color: #30A9DC;\n" +
                "        border-left-color: #30A9DC;\n" +
                "        border-top-color: #EE8A21;\n" +
                "        border-right-color: #EE8A21;\n" +
                "        position: absolute;\n" +
                "        top: 15px;\n" +
                "        right: 15px;\n" +
                "        width: 150px;\n" +
                "        padding: 10px;\n" +
                "        border-radius: 50%;\n" +
                "        -moz-border-radius: 50%;\n" +
                "        -webkit-border-radius: 50%;\n" +
                "      }\n" +
                "      .container{\n" +
                "        width: 50%;\n" +
                "      }\n" +
                "      h1{\n" +
                "        color: #30A9DC; font-size: 30px;\n" +
                "        font-family: Times;\n" +
                "        text-align: left;\n" +
                "        font-size: 42px;\n" +
                "      }\n" +
                "      #canvas{\n" +
                "        width: 50%;\n" +
                "      }\n" +
                "    </style>\n" +
                "\n" +
                "    <meta name=\"viewport\" content=\"initilal-scale=1.0, maximum-scale=1.0, user-scalable=1\">\n" +
                "    <title>DW2022/B/T5_GROUP</title>\n" +
                "  </head>\n" +
                "\n" +
                "  <body>\n" +
                "    <div class=\"container\">\n" +
                "    <h1><b>APPLICAZIONE IN BORSA</b></h1>\n" +
                "    <h4>Ticker: "+ticker+"</h4>\n" +
                "    <h4>Exchange: "+sid.getExchange()+"</h4>\n" +
                "    <h4>Name: "+sid.getName()+"</h4>\n" +
                "    <h4>HeadQuarter Address: "+sid.getHqAddress()+"</h4>\n" +
                "    <h4>Country: "+sid.getHqCountry()+"</h4>\n" +
                "    <h4>Description: "+sid.getDescription()+"</h4>\n" +
                "    <br>\n"+
                "    <h4>The theoretical max achievable is: "+cd.getTheoricalMaxAch()+"</h4>\n" +
                "    <h4>The growing days percentage is: "+cd.getGrowDayPerc()+"</h4>\n" +
                "    <h4>The total time profit is: "+cd.getProfitTotalTime()+"</h4>\n" +
                "    <img src=\"../../doc/LogoDWT5.png\" border=\"4.9px\">\n" +
                "      <div>\n" +
                "        <canvas id=\"canvas1\"></canvas>\n" +
                "        <canvas id=\"canvas2\"></canvas>\n" +
                "      </div>\n" +
                "    </div>\n" +
                "    <script type=\"text/javascript\" src=\"https://cdn.jsdelivr.net/npm/chart.js@3.7.0/dist/chart.min.js\"></script>\n" +
                "    <script type=\"text/javascript\">\n" +
                "var ctx = document.getElementById('canvas1').getContext('2d');\n" +
                "      var myChart = new Chart(ctx, {\n" +
                "        type: 'line',\n" +
                "        data: {\n" +
                "            labels: ["+cd.getDaysInProg()+"],\n" +
                "            datasets: [{\n" +
                "                label: 'Stock Close',\n" +
                "                data: ["+cd.getAverage()+"],\n" +
                "                backgroundColor:'trasparent',\n" +
                "                borderColor:'#30A9DC',\n" +
                "                borderWidth: 4\n" +
                "            }],\n" +
                "          },"+
                "        options: {\n" +
                "            scales: {\n" +
                "                y: {\n" +
                "                    beginAtZero: true\n" +
                "                }\n" +
                "            }\n" +
                "        }\n" +
                "        });\n" +

                "var ctx1 = document.getElementById('canvas2').getContext('2d');\n" +
                        "        var secondChart = new Chart(ctx1, {\n" +
                        "          type: 'line',\n" +
                        "          data: {\n" +
                        "              labels: ["+cd.getDays()+"],\n" +
                        "              datasets: [{\n" +
                        "                  label: 'Trend',\n" +
                        "                  data: ["+cd.getDaysData()+"],\n" +
                        "                  backgroundColor:'trasparent',\n" +
                        "                  borderColor:'#EE8A21',\n" +
                        "                  borderWidth: 4\n" +
                        "              }],\n" +
                        "            },        options: {\n" +
                        "              scales: {\n" +
                        "                  y: {\n" +
                        "                      beginAtZero: true\n" +
                        "                  }\n" +
                        "              }\n" +
                        "          }\n" +
                        "          });"+
                "setTimeout(\"location.reload(true);\", 11000);"+
                "    </script>\n" +
                "  </body>\n" +
                "</html>"
        );
        bw.close();
    }
}
