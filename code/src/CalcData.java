public class CalcData {
    private double theoricalMaxAch;
    private double profitDay;
    private double profitTotalTime;
    private double growDayPerc;
    private String daysData;
    private String days;
    private String average;
    private String daysInProg;


    public String getDaysInProg() { return daysInProg; }

    public void setDaysInProg(String daysInProg) { this.daysInProg = daysInProg; }

    public String getAverage() { return average; }

    public void setAverage(String average) { this.average = average; }

    public String getDays() { return days; }

    public void setDays(String days) { this.days = days; }

    public String getDaysData() { return daysData; }

    public void setDaysData(String daysData) { this.daysData = daysData; }

    public double getTheoricalMaxAch() {
        return theoricalMaxAch;
    }

    public void setTheoricalMaxAch(double theoricalMaxAch) {
        this.theoricalMaxAch = theoricalMaxAch;
    }

    public double getProfitDay() {
        return profitDay;
    }

    public void setProfitDay(double profitDay) {
        this.profitDay = profitDay;
    }

    public double getProfitTotalTime() {
        return profitTotalTime;
    }

    public void setProfitTotalTime(double profitTotalTime) {
        this.profitTotalTime = profitTotalTime;
    }

    public double getGrowDayPerc() {
        return growDayPerc;
    }

    public void setGrowDayPerc(double growDayPerc) {
        this.growDayPerc = growDayPerc;
    }
}
