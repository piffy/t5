import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/* this class gets from a rest api url a json string, and after that, it convert the string in a key-value map
 * you could know the ticker and the starting date of the market stock that you want to know data, and youve to put them into
 * the readStockData arguments. If you want to get the specific data about the stock, you could call the get method of that data
 */
public class RestQuery {
    private ScriptEngine engine;
    public RestQuery(){
        initEngine();//lo metto qui perchè chiamandolo dentro parseJson si sprecherebbe molto tempo per ogni chiamata.
    }
    /* Queries the REST API, populates the fileds, retries after a small delay if the server is too busy at the moment
     * @param ticker
     * @param startingDate
     * @throws Exception if the requested Date is invalid, so please skip it.
     */
    public void readStockData(String ticker, Date startingDate, StockData sd) throws Exception{
        final String NEW_FORMAT = "yyyy-MM-dd";
        DateFormat df = new SimpleDateFormat(NEW_FORMAT);
        String formattedStartingDate = df.format(startingDate);
        URL url = new URL("https://api.polygon.io/v1/open-close/"+ticker+"/"+formattedStartingDate+"?adjusted=true&apiKey=wjotgaH7mIpLQXaK3MFmKqKSVR7hijB6");
        int status=0;
        CalcData cd = new CalcData();
        do{
            Thread.sleep(10000);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            con.setRequestProperty("Content-Type", "application/json");
            String contentType = con.getHeaderField("Content-Type");
            con.setConnectTimeout(5000);
            con.setReadTimeout(5000);
            status = con.getResponseCode();
            if (status==200){
                //HTTP OK
                BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
                String inputLine;
                StringBuffer content = new StringBuffer();
                while ((inputLine = in.readLine()) != null) {
                    content.append(inputLine);
                }
                in.close();
                con.disconnect();
                String json =content.toString();
                //System.out.println(json);
                Map values = parseJson(json);
                //System.out.println(values.get("symbol"));
                sd.setStockOpen(asDouble(values.get("open")));
                sd.setStockHigh(asDouble(values.get("high")));
                sd.setStockLow(asDouble(values.get("low")));
                sd.setStockClose(asDouble(values.get("close")));
                sd.setStockVolume(asDouble(values.get("volume")));
                sd.setStockPreMarket(asDouble(values.get("preMarket")));
                sd.setStockAfterHours(asDouble(values.get("afterHours")));
                //System.out.println(ticker);
            } else if (status==429){
                System.out.println("Server too busy: waiting and retrying...");
            } else {
                throw new Exception("borsa chiusa");
            }
        }while(status==429);
    }
    private void initEngine() {
        ScriptEngineManager sem = new ScriptEngineManager();
        this.engine = sem.getEngineByName("javascript");
    }
    /* The parseJson method returns a Map with Integer or Double objects depending on the presence of the decimal dot
     * in the json string. This method ensures a conversion to double based on string representation,

     luca marolda, [10/02/2022 20:58]
     despite any
     * kind of object

     returned by the map
     * @param o any
    o
    bject
     * @return the double vers
    i
    on of its string representatio
    n

     */
    private double asDouble(Object o){
        return Double.parseDouble(o.toString());

    }
    public Date asDate(Object o) throws ParseException {
        String s = o.toString();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        return sdf.parse(s);
    }

    /* this method, with a script and a engine builds a map of a json string
     * @param json my json string
     * @return Map that contains json key and values
     * @throws IOException
     *

             @throws ScriptException
     */
    private Map parseJson(String json) throws IOException, ScriptException {
        String script ="Java.asJSONCompatible("+ json + ")";
        Object result = this.engine.eval(script);
        //assertThat(result, instanceOf(Map.class));
        Map contents = (Map) result;
        //contents.forEach(( t, u) ->{
        //key-value pairs
        //});
        return contents;
    }
    public void readInputData(InputData id)throws Exception{


        URL url = new URL("https://dwweb.gnet.it/dw2022/");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        String contentType = con.getHeaderField("Content-Type");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        String json =content.toString();
        //System.out.println(json);
        Map values = parseJson(json);
        id.setTicker(values.get("ticker").toString());
        id.setStartingDate(asDate(values.get("starting_date")));
        id.setEndingDate(asDate(values.get("ending_date")));
    }

    public void readStockInfo(StockInfoData sid, String ticker) throws Exception{
        URL url = new URL("https://api.polygon.io/v1/meta/symbols/"+ticker+"/company?apiKey=iPu0RlENAL5gxcdnvbyisASmoLxCghaC");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("Content-Type", "application/json");
        String contentType = con.getHeaderField("Content-Type");
        con.setConnectTimeout(5000);
        con.setReadTimeout(5000);
        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer content = new StringBuffer();
        while ((inputLine = in.readLine()) != null) {
            content.append(inputLine);
        }
        in.close();
        con.disconnect();
        String json =content.toString();
        Map values = parseJson(json);
        sid.setExchange(values.get("exchange").toString());
        sid.setName(values.get("name").toString());
        sid.setHqAddress(values.get("hq_address").toString());
        sid.setHqCountry(values.get("hq_country").toString());
        sid.setDescription(values.get("description").toString());

        System.out.println(sid.getExchange());
        System.out.println(sid.getName());
        System.out.println(sid.getHqAddress());
        System.out.println(sid.getHqCountry());
        System.out.println(sid.getDescription());
    }
}