import java.awt.*;
import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
public class Calc {
    private double theoricalMaxAch;
    private double profitDay;
    private double profitTotalTime;
    private double growDayPerc;
    private double dayIncreased=0;
    private double totalDays=0;
    private String daysData="";

    private final String REQUESTED_FORMAT = "dd/MM/yyyy";
    private DateFormat df = new SimpleDateFormat(REQUESTED_FORMAT);
    private HtmlOutput htmlOutput = new HtmlOutput();

    public void calcValues(String ticker, Date startingDate, Date endingDate, CalcData cd, StockInfoData sid){
        String daysInProg="";
        String average="";
        RestQuery rest = new RestQuery();
        Calendar c = Calendar.getInstance();
        c.setTime(startingDate);
        StockData sd = new StockData();
        Rolling stones = new Rolling(4);
        do{
            try{
                rest.readStockData(ticker,c.getTime(), sd);
                theoricalMaxAch += sd.getStockHigh()-sd.getStockLow();
                profitDay = sd.getStockOpen() - sd.getStockClose();
                profitTotalTime += profitDay;
                totalDays++;
                daysInProg+="'"+df.format(c.getTime())+"',";

                if(profitDay>0) {
                    dayIncreased++;
                }

                daysData+=profitDay+",";

                stones.add(sd.getStockClose());
                average+=stones.getAverage()+",";
                growDayPerc = (dayIncreased/totalDays)*100;

                cd.setTheoricalMaxAch(theoricalMaxAch);
                cd.setProfitDay(profitDay);
                cd.setAverage(String.valueOf(average));
                cd.setDaysData(String.valueOf(daysData));
                cd.setDaysInProg(daysInProg);
                cd.setProfitTotalTime(profitTotalTime);
                cd.setGrowDayPerc(growDayPerc);

                System.out.println(c.getTime());
                System.out.println(ticker);
                System.out.println("Massimo teorico realizzabile "+cd.getTheoricalMaxAch());
                System.out.println("Profitto del giorno corrente "+cd.getProfitDay());
                System.out.println("Percentuale giorni in crescita "+cd.getGrowDayPerc()+"%\n");



                File f = new File("DW2022_B_T5_GROUP.html");
                calcDays(endingDate, startingDate, cd);
                htmlOutput.writeHtml(f,cd,ticker,sid,c);

                if(c.getTime().equals(startingDate)) {
                    Desktop.getDesktop().browse(f.toURI());
                }

            }catch (Exception e) {
                e.printStackTrace();
            }
            c.add(Calendar.DATE, 1);
        }while(!c.getTime().equals(endingDate));

        System.out.println("Profitto totale "+cd.getProfitTotalTime());
    }

    private void calcDays(Date endingDate, Date startingDate, CalcData cd){
        Calendar c = Calendar.getInstance();
        String days="'"+df.format(startingDate)+"',";
        c.setTime(startingDate);
        do {
            c.add(Calendar.DATE, 1);
            days+="'"+df.format(c.getTime())+"',";
        }while (!c.getTime().equals(endingDate));

        cd.setDays(days);
    }
}