Valutazione Torricelli: 3.86 (seconda, di poco)
Evoluzione progettazione:La prima versione era abbastanza diversa, pur mostrando alcune impostazioni di base corrette. L'ultima versione è molto ricca, ma suona un po' di retroject. Il grafico è completo e abbastanza corretto (qualche freccia è strana)


Critica UML
Il progetto è strutturato, fin troppo. Ci sono alcune classi artificiali, come le *Data, che di fatto sono solo un modo per passare parametri. Altre classi sono bene strutturate. In generale un lavoro apprezzabile. 


Critica Codice
Static code Analysis: A

*Data.java: Classi/contenitore, senza logica

Rolling.java: Media mobile. Carina, ma probabilmente trovata in giro.

HtmlOutput.java: classe di servizio, bruttina. Si poteva leggere il file e poi modificarlo con le RegExp, oppure usare un template engine.

Calc.java: fa i vari calcoli. OK

RestQuery: cuore del sistema e ben organizzato









 




	
	
