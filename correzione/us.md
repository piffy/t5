User stories totali (fatte) 7/7 
Commento US: Generalmente buone. Manca qualsiasi riferimento al fatto che l'input derivi dalla chiamate rest al server; il log evidenzia una grande attività di aggiornamento gestito da più persone. User è troppo generico: "investor", "speculator", "curios", "Statistic maniac". Esplicitare il motivo sarebbe servito. Stimate: no, Prioritizzate: no.
Organizzazione in sprint (velocity): si -   3/4/0, circa 1.2 al giorno - forse US un po' grosse  

CRITICA
As a user i want to be able to read the results of the application on my favourite browser
	THIS IS EITHER TOO BIG OR TOO EASY
As a user I would like to be able to get specific stock market data for a specific time interval
	OK
As a user I would like to get some calculations on the stock values over the specified period (theor/max/achvbl, gain/loss, grow%)
	YOU COULD HAVE DIVIDED IT INTO 3
As a user I would like to know the trend of a specific stock on the stock exchange with use of charts
	A LITTLE TOO GENERIC - SPECIFIC... HOW?
As a user i want a data refresh every 2 mins.
	WASN'T IT 30 SEC?
OPTIONAL: As a developer I want the user to have a great HTML page with CSS to improve the graphics of the page
	NOT A USER STORY SINCE IT'S UNTESTABLE AND SUBJECTIVE
As a user I want to be able to visualize: The code of the requested stock, The market where it is traded (e.g. NASDAQ), The name of the company, The address of its headquarters, The description of the activity.
	OK YOU COULD HAD IT WORDED IN A MORE CONCISE WAY


