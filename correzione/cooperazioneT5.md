# TEAMWORK

## Impegno (ore personali non esplicitate)
Luca Marolda: Manager, Developer lvl.1, Trello maintainer.
Nicolò Simonini; Developer lvl.1, Web Developer.
Leonardo Partesotti: Developer lvl.2, Tester, UML, Doc Writer, Web Developer, Product Owner.
Jacopo Scarpato: Developer lvl.2, Tester, UML, Trello maintainer, Scrum Master.
Ziad Majid: Developer lvl.3, UML, Supervisor, Doc writer.



## Git 
*Usato il tag corretto per il PP*
     Leonardo Partesotti 24
     Luca Marolda 30
     Scarpato_J 14
     Ziad Majid 11
     Jacopo Scarpato 8
     Simonini 11
 
 
Web upload: 0/100

## Retrospective
Eseguita in modo non chiaro, di scarsa efficacia. Nella seconda non ci sono proposte attuabili


## Sintesi
Il quadro d'insieme indica un gruppo che ha saputo organizzarsi e collaborare fin da subito, mitigando così le differenze di impegno e di capacità e interesse. Il gruppo ha sfruttato bene le nuove tecnologie, ed ha fallito il successo annunciato per un soffio - al gruppo la risposta a questo enigma. 







