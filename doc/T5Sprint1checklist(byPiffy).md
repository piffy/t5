# T5 Sprint 1 checklist

* Documentazione di progettazione aggiornata: OK (meglio di prima)
* Sprint backlog aggiornato su trello: 1US completata, altre iniziate
* Codice : OK (no syntax errore, richiede Java 1.8)
* Codice JUnit : NO
* JavaDoc: NO
* Documentazione in formato gittabile (grafici esclusi) : NO (bonus)
* Diario giornaliero delle attività personali (incluse ore fuori orario) : NO (bonus)
* Daily status: OK
* Review video: OK 
* Retrospettiva : PENDING



